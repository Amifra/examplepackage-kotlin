[main](../../index.md) / [space.airisius.kotlinexample](../index.md) / [ExamplePlugin](index.md) / [&lt;init&gt;](./-init-.md)

# &lt;init&gt;

`ExamplePlugin()`

This class represents plugin that can register event listeners, commands; interact with the
configuration, etc.

