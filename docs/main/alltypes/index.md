

### All Types

| Name | Summary |
|---|---|
| [space.airisius.kotlinexample.ExamplePackage](../space.airisius.kotlinexample/-example-package/index.md) | Package class that registers included plugins |
| [space.airisius.kotlinexample.ExamplePlugin](../space.airisius.kotlinexample/-example-plugin/index.md) | This class represents plugin that can register event listeners, commands; interact with the configuration, etc. |
