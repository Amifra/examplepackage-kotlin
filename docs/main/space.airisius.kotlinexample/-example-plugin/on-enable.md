[main](../../index.md) / [space.airisius.kotlinexample](../index.md) / [ExamplePlugin](index.md) / [onEnable](./on-enable.md)

# onEnable

`fun onEnable(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Enter point of the plugin

