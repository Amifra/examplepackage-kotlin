package space.airisius.kotlinexample

import airisius.core.pkg.AirisiusPackage
import airisius.core.plugin.PluginsManager

/**
 * Package class that registers included plugins
 */
@Suppress("unused")
class ExamplePackage : AirisiusPackage() {
  /**
   * Entry point
   */
  override fun main() {
    PluginsManager.addPlugin(ExamplePlugin::class.java)
  }
}
