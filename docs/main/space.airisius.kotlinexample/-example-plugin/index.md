[main](../../index.md) / [space.airisius.kotlinexample](../index.md) / [ExamplePlugin](./index.md)

# ExamplePlugin

`class ExamplePlugin : AirisiusPlugin`

This class represents plugin that can register event listeners, commands; interact with the
configuration, etc.

### Constructors

| Name | Summary |
|---|---|
| [&lt;init&gt;](-init-.md) | `ExamplePlugin()`<br>This class represents plugin that can register event listeners, commands; interact with the configuration, etc. |

### Functions

| Name | Summary |
|---|---|
| [getName](get-name.md) | `fun getName(): `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)<br>The getter for the plugin's name that you should override |
| [onEnable](on-enable.md) | `fun onEnable(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Enter point of the plugin |
