package space.airisius.kotlinexample

import airisius.core.command.AirisiusCommand
import airisius.core.event.AirisiusListener
import airisius.core.plugin.AirisiusPlugin
import com.google.common.collect.Lists
import org.bukkit.event.player.PlayerJoinEvent

/**
 * This class represents plugin that can register event listeners, commands; interact with the
 * configuration, etc.
 */
class ExamplePlugin : AirisiusPlugin() {

  /**
   * The getter for the plugin's name that you should override
   *
   * @return plugin's name
   */
  override fun getName(): String {
    return "Example"
  }

  /**
   * Entry point of the plugin
   */
  override fun onEnable() {

    listener = AirisiusListener().listen(PlayerJoinEvent::class.java) { event ->
      event.player.sendMessage(config.getString("text"))
    }

    addCommand(
      AirisiusCommand("example").setExecutor { sender, _ -> sender.sendMessage(config.getString("text") as String) }.setTabCompleter { _, _ -> Lists.newArrayList() }
    )

    addConfig("text", "Some example text")
  }
}