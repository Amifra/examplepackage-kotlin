[main](../../index.md) / [space.airisius.kotlinexample](../index.md) / [ExamplePackage](index.md) / [main](./main.md)

# main

`fun main(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Enter point

