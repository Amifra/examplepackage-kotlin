[main](../index.md) / [space.airisius.kotlinexample](./index.md)

## Package space.airisius.kotlinexample


This is the package

### Types

| Name | Summary |
|---|---|
| [ExamplePackage](-example-package/index.md) | `class ExamplePackage : AirisiusPackage`<br>Package class that registers included plugins |
| [ExamplePlugin](-example-plugin/index.md) | `class ExamplePlugin : AirisiusPlugin`<br>This class represents plugin that can register event listeners, commands; interact with the configuration, etc. |
