[main](../../index.md) / [space.airisius.kotlinexample](../index.md) / [ExamplePlugin](index.md) / [getName](./get-name.md)

# getName

`fun getName(): `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

The getter for the plugin's name that you should override

**Return**
plugin's name

