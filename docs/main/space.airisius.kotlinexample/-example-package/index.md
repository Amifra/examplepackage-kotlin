[main](../../index.md) / [space.airisius.kotlinexample](../index.md) / [ExamplePackage](./index.md)

# ExamplePackage

`class ExamplePackage : AirisiusPackage`

Package class that registers included plugins

### Constructors

| Name | Summary |
|---|---|
| [&lt;init&gt;](-init-.md) | `ExamplePackage()`<br>Package class that registers included plugins |

### Functions

| Name | Summary |
|---|---|
| [main](main.md) | `fun main(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Enter point |
